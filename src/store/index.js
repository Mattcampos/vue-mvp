import { createStore } from "vuex";

export default createStore({
  state: {
    jwt: undefined
  },
  mutations: {
    initialiseStore(state) {
      if (localStorage.getItem('store')) {
        Object.assign(state, JSON.parse(localStorage.getItem('store')))
      }
    },
    saveCredential(state, credential) {
      state.jwt = credential
    }
  },
  actions: {},
  modules: {}
});
