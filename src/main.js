import { createApp } from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";

import 'bootstrap/dist/css/bootstrap.css'

createApp(App)
  .use(store)
  .use(router)
  .mount("#app");

store.subscribe((mutation, state) => {
  localStorage.setItem('store', JSON.stringify(state))
})

store.commit('initialiseStore')
